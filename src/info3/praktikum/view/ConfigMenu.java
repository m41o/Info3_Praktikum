package info3.praktikum.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import hsrt.mec.controldeveloper.core.com.command.ICommand;

import info3.praktikum.model.Command;
import info3.praktikum.model.CommandType;

/**
 * GUI component for configuring commands in the list
 *
 * @author Markus Lochmann
 *
 */
@SuppressWarnings("serial")
public class ConfigMenu extends JDialog {
  /**
   * Constant that sets the menu to add a new command to the chain
   */
  private static final int ADD_MENU = 0;
  
  /**
   * Constant that sets the menu to change the selected command
   */
  private static final int CONFIG_MENU = 1;
  
  //Constants determining the window size
  private static final int HEIGHT = 200;
  private static final int WIDTH = 300;
  
  //Java Swing components
  private JButton okButton = new JButton();
  private JButton cancelButton = new JButton();
  private JComboBox<CommandType> commandTypes;
  private JPanel centerPanel;
  private JPanel southPanel;
  private JPanel westPanel;
  private JTextField tf1 = new JTextField(8);
  private JTextField tf2 = new JTextField(8);
  private JLabel label1 = new JLabel();
  private JLabel label2 = new JLabel();
  
  /**
   * Currently selected command in the table
   */
  private ICommand selection = null;
  
  /**
   * Parent of this dialog (main window of ControDeveloper) 
   */
  private ControlDeveloper owner;
  
  /**
   * Integer determining the dialog's behavior
   */
  private int behaviour;

  /**
   * Creates a new ConfigMenu dialog for configuring a selected command
   *
   * @param owner JFrame that invokes the dialog
   * @param title Title for the dialog
   * @param selection Currently selected command in the table
   */
  public ConfigMenu(ControlDeveloper owner, String title, ICommand selection) {
    super(owner, title, true);
    this.owner = owner;
    this.behaviour = CONFIG_MENU;
    this.selection = selection;
    setUpDialog();
    commandTypes.setSelectedIndex(((Command) selection).getIndex());
    showParams((Command) selection);
  }

  /**
   * Creates a new ConfigMenu dialog for adding a command
   *
   * @param owner JFrame that invokes the dialog
   * @param title Title for the dialog
   */
  public ConfigMenu(ControlDeveloper owner, String title) {
    super(owner, title, true);
    this.owner = owner;
    this.behaviour = ADD_MENU;

    setUpDialog();
    showParams(getCommand());
  }

  /**
   * Set up dialog
   */
  private void setUpDialog() {
    setLayout(new BorderLayout());
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    setSize(WIDTH, HEIGHT);
    setLocationRelativeTo(owner);
    setResizable(false);
    setUpSouthPanel();
    setUpComboBox(owner);
    setUpCenterPanel();
  }

  /**
   * Set up centerPanel
   */
  private void setUpCenterPanel() {
    centerPanel = new JPanel();
    GroupLayout layout = new GroupLayout(centerPanel);
    centerPanel.setLayout(layout);
    centerPanel.setBorder(BorderFactory.createTitledBorder("Parameters"));
    
    //tf1.addFocusListener(new TextFieldFocusListener(TextFieldFocusListener.FIRST_PARAM));
    //tf2.addFocusListener(new TextFieldFocusListener(TextFieldFocusListener.SECOND_PARAM));
    tf1.setInputVerifier(new TextFieldInputVerifier(TextFieldInputVerifier.FIRST_PARAM));
    tf2.setInputVerifier(new TextFieldInputVerifier(TextFieldInputVerifier.SECOND_PARAM));

    // Turn on automatically adding gaps between components
    layout.setAutoCreateGaps(true);

    // Turn on automatically creating gaps between components that touch
    // the edge of the container.
    layout.setAutoCreateContainerGaps(true);

    // Create a sequential group for the horizontal axis.
    GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();

    // The sequential group in turn contains two parallel groups.
    // One parallel group contains the labels, the other the text fields.
    // Putting the labels in a parallel group along the horizontal axis
    // positions them at the same x location.
    // Variable indentation is used to reinforce the level of grouping.
    hGroup.addGroup(layout.createParallelGroup().addComponent(label1).addComponent(label2));
    hGroup.addGroup(layout.createParallelGroup().addComponent(tf1).addComponent(tf2));
    layout.setHorizontalGroup(hGroup);

    // Create a sequential group for the vertical axis.
    GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();

    // The sequential group contains two parallel groups that align
    // the contents along the baseline. The first parallel group contains
    // the first label and text field, and the second parallel group contains
    // the second label and text field. By using a sequential group
    // the labels and text fields are positioned vertically after one another.
    vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(label1)
        .addComponent(tf1));
    vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(label2)
        .addComponent(tf2));
    layout.setVerticalGroup(vGroup);

    add(centerPanel, BorderLayout.CENTER);
  }

  /**
   * Configure and add buttons to southPanel
   */
  private void setUpSouthPanel() {
    southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    setUpOkButton();
    setUpCancelButton();
    add(southPanel, BorderLayout.SOUTH);
  }

  /**
   * Set up OK button
   */
  private void setUpOkButton() {
    if (behaviour == ADD_MENU)
      okButton.setText("Add");
    else if (behaviour == CONFIG_MENU)
      okButton.setText("Save");

    okButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        Command cmd = getCommand();

        if (cmd != null) {
          Number[] params = {0.0, 0.0};

          try {
            params[0] = new Double(tf1.getText());
          } catch (Exception e) {
          }

          try {
            params[1] = new Double(tf2.getText());
          } catch (Exception e) {
          }

          cmd.setParams(params);

          if (behaviour == CONFIG_MENU)
            owner.model.replace(selection, (ICommand) cmd);
          else if (behaviour == ADD_MENU)
            owner.model.add((ICommand) cmd);
        }

        dispose();
      }
    });

    southPanel.add(okButton);
    getRootPane().setDefaultButton(okButton);
  }

  /**
   * Set up cancel button
   */
  private void setUpCancelButton() {
    cancelButton.setText("Cancel");
    cancelButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        dispose();
      }
    });
    southPanel.add(cancelButton);
  }

  /**
   * Set up the combo box for CommandTypes
   *
   * @param owner Owner of the ConfigMenu dialog
   */
  private void setUpComboBox(ControlDeveloper owner) {
    commandTypes = new JComboBox<CommandType>(owner.model.getCommandTypes());
    commandTypes.setEditable(false);
    commandTypes.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        showParams(getCommand());
      }
    });

    westPanel = new JPanel();
    westPanel.setBorder(BorderFactory.createTitledBorder("Command Type"));
    westPanel.add(commandTypes);
    add(westPanel, BorderLayout.WEST);
  }

  /**
   * Display parameters for selected command on centerPanel
   *
   * @param cmd Currently selected command
   */
  private void showParams(Command cmd) {

    // for every parameter in cmd.params set the Label and TextField
    String[] params = cmd.getParams();

    label1.setText(params[0] + ":");
    tf1.setText(cmd.getFirstParam().toString());

    if (params.length == 1) {
      label2.setVisible(false);
      tf2.setVisible(false);
    } else {
      label2.setText(params[1] + ":");
      tf2.setText(cmd.getSecondParam().toString());

      label2.setVisible(true);
      tf2.setVisible(true);
    }
  }

  /**
   * Get a Command object based on current selection in combo box
   *
   * @return A command instance of the current selection
   */
  private Command getCommand() {
    CommandType cT = (CommandType) commandTypes.getSelectedItem();
    return (Command) cT.createInstance();
  }
  
  /**
   * Listener that checks and automatically corrects the value in a TextField when focus is lost
   * 
   * @author Markus Lochmann
   *
   */
  private class TextFieldFocusListener extends FocusAdapter {
    public static final int FIRST_PARAM = 0;
    public static final int SECOND_PARAM = 1;
    private int paramNum;
    
    /**
     * Constructs a new FocusListener object
     * @param paramNum Determines whether to compare the TextField to first or second parameter of a command 
     */
    public TextFieldFocusListener(int paramNum) {
      super();
      this.paramNum = paramNum;
    }
    
    /**
     * Method that is called when focus on the TextField is lost
     * @param e the FocusEvent that triggered execution
     */
    public void focusLost(FocusEvent e) {
      JTextField source = null;
      Number max = null;
      Number min = null;
      
      if (e.getComponent() instanceof JTextField)
        source = (JTextField) e.getComponent();
      else
        return;
      
      Number value = null;
      
      try {
        value = Double.parseDouble(source.getText());
      }
      catch (NumberFormatException ex) {
        source.setText("0");
        return;
      }
      
      if (paramNum == FIRST_PARAM) {
        max = getCommand().getFirstParamMax();
        min = getCommand().getFirstParamMin();
      }
      else if (paramNum == SECOND_PARAM) {
        max = getCommand().getSecondParamMax();
        min = getCommand().getSecondParamMin();
      }
      else
        return;
      
      
      if (max != null && max.doubleValue() < value.doubleValue()) {
          source.setText(max.toString());
          return;
      }    
      else if (min != null && min.doubleValue() > value.doubleValue()) {
        source.setText(min.toString());
        return;
      }        
    }
  }
  
  /**
   * Verifier that verifies whether the value in a TextField is within the bounds
   * for a parameter of the selected command type
   *  
   * @author Markus Lochmann
   *
   */
  private class TextFieldInputVerifier extends InputVerifier {
    public static final int FIRST_PARAM = 0;
    public static final int SECOND_PARAM = 1;
    private int paramNum;
    
    /**
     * Constructs a new InputVerifier object
     * @param paramNum Determines whether to compare the TextField to first or second parameter of a command
     */
    public TextFieldInputVerifier(int paramNum) {
      super();
      this.paramNum = paramNum;
    }

    /**
     * Method that verifies whether the input is correct
     * @param arg0 JComponent the verifier is attached to
     * @return true if input is correct, false if input is incorrect
     */
    @Override
    public boolean verify(JComponent arg0) {
      JTextField source = null;
      Number max = null;
      Number min = null;
      
      if (arg0 instanceof JTextField)
        source = (JTextField) arg0;
      else
        return true;
      
      Number value = null;
      
      try {
        value = Double.parseDouble(source.getText());
      }
      catch (NumberFormatException ex) {
        return false;
      }
      
      if (paramNum == FIRST_PARAM) {
        max = getCommand().getFirstParamMax();
        min = getCommand().getFirstParamMin();
      }
      else if (paramNum == SECOND_PARAM) {
        max = getCommand().getSecondParamMax();
        min = getCommand().getSecondParamMin();
      }
      else
        return true;
      
      
      if (max != null && max.doubleValue() < value.doubleValue()) {
          source.setText(max.toString());
          return true;
      }    
      else if (min != null && min.doubleValue() > value.doubleValue()) {
        source.setText(min.toString());
        return true;
      }
      return true;
    }
  }
}
