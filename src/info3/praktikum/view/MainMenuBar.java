package info3.praktikum.view;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * MenuBar component of ControlDeveloper
 *
 * @author Christoph Jabs
 * @author Markus Lochmann
 *
 */
@SuppressWarnings("serial")
class MainMenuBar extends JMenuBar {
  private ControlDeveloper parent;
  private JMenu fileMenu = new JMenu("File");
  private JMenu optionsMenu = new JMenu("Options");
  private JMenu helpMenu = new JMenu("Help");

  private JMenuItem saveMenuItem = new JMenuItem("Save");
  private JMenuItem loadMenuItem = new JMenuItem("Load");
  private JMenuItem loadAppendMenuItem = new JMenuItem("Load and append");
  private JMenuItem wifiMenuItem = new JMenuItem("Wifi Settings");
  private JMenuItem loggingMenuItem = new JMenuItem("Logging Settings");
  private JMenuItem aboutMenuItem = new JMenuItem("About");
  // File chooser
  private JFileChooser fileChooser = new JFileChooser();
  private FileNameExtensionFilter textFileFilter = new FileNameExtensionFilter("Text files", "txt");
  private FileNameExtensionFilter objectFileFilter =
      new FileNameExtensionFilter("Object files", "o", "ser");

  public MainMenuBar(ControlDeveloper parent) {
    this.parent = parent;
    configureFileMenu();
    configureOptionsMenu();
    configureInfoMenu();
    buildMenuBar();
  }

  /**
   * Configure the individual components of the File Menu
   */
  private void configureFileMenu() {
    // Menu items
    saveMenuItem.setAccelerator(
        KeyStroke.getKeyStroke('S', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
    loadMenuItem.setAccelerator(
        KeyStroke.getKeyStroke('O', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
    loadAppendMenuItem.setAccelerator(
        KeyStroke.getKeyStroke('O', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()
            | java.awt.event.InputEvent.SHIFT_DOWN_MASK));

    fileChooser.setAcceptAllFileFilterUsed(false);
    fileChooser.setFileFilter(textFileFilter);
    fileChooser.addChoosableFileFilter(objectFileFilter);

    saveMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (fileChooser.showSaveDialog(parent) == JFileChooser.APPROVE_OPTION)
          parent.model.save(fileChooser.getSelectedFile(), false,
              fileChooser.getFileFilter() == objectFileFilter);
      }
    });

    loadMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (fileChooser.showOpenDialog(parent) == JFileChooser.APPROVE_OPTION)
          parent.model.load(fileChooser.getSelectedFile(),
              fileChooser.getFileFilter() == objectFileFilter);
      }
    });

    loadAppendMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (fileChooser.showDialog(parent, "Append") == JFileChooser.APPROVE_OPTION)
          parent.model.load(fileChooser.getSelectedFile(), true,
              fileChooser.getFileFilter() == objectFileFilter);
      }
    });
  }

  /**
   * Configure the individual components of the Options Menu
   */
  private void configureOptionsMenu() {
    wifiMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        new WifiMenu(parent);
      }
    });

    loggingMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        LogMenu logMenu = new LogMenu(parent, "Log Menu");
        logMenu.setVisible(true);
      }
    });
  }

  /**
   * Configure components of the Info menu
   */
  private void configureInfoMenu() {
    aboutMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        AboutMenu.createAboutMenu(parent, "About " + parent.getTitle());
      }
    });
  }

  /**
   * Add the components to the Menu
   */
  private void buildMenuBar() {
    // File menu
    fileMenu.add(saveMenuItem);
    fileMenu.add(loadMenuItem);
    fileMenu.add(loadAppendMenuItem);

    // Options menu
    optionsMenu.add(wifiMenuItem);
    optionsMenu.add(loggingMenuItem);

    // Info menu
    helpMenu.add(aboutMenuItem);

    // Menu bar
    add(fileMenu);
    add(optionsMenu);
    add(helpMenu);
  }
}
