package info3.praktikum.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Dialog for configuring logging options
 *
 * @author Christoph Jabs (christoph@jabs-family.de)
 */
@SuppressWarnings("serial")
public class LogMenu extends JDialog {
  // GUI Components
  // Panes
  private JPanel guiPane = new JPanel();
  private JPanel filePane = new JPanel();
  private JPanel buttonPane = new JPanel();
  private JPanel pathPane = new JPanel();
  private JPanel formatterPane = new JPanel();
  // Checkboxes
  private JCheckBox guiLogCheck = new JCheckBox("Show log in GUI");
  private JCheckBox fileLogCheck = new JCheckBox("Save log to file");
  // Radio buttons
  private JRadioButton xmlButton = new JRadioButton("XML formatting");
  private JRadioButton simpleButton = new JRadioButton("Simple formatting");
  private ButtonGroup formatterButtons = new ButtonGroup();
  // Buttons
  private JButton cancleButton = new JButton("Cancel");
  private JButton okButton = new JButton("OK");
  private JButton browseButton = new JButton("...");
  // Text field
  private JTextField pathText = new JTextField();
  // File chooser
  private JFileChooser fileChooser = new JFileChooser();
  private FileNameExtensionFilter logFileFilter = new FileNameExtensionFilter("Log files", "log");

  private ControlDeveloper owner;
  private JDialog parent = this;

  /**
   * Construct new dialog connected to owner
   *
   * @param owner The ControlDeveloper that invokes the dialog
   * @param title The title of the dialog
   */
  public LogMenu(ControlDeveloper owner, String title) {
    super(owner, title, true);
    this.owner = owner;
    configureComponents();
    buildWindow();
  }

  /**
   * Configures all the individual components
   */
  private void configureComponents() {
    // GUI panel
    guiPane.setBorder(BorderFactory.createTitledBorder("GUI logging"));
    guiPane.setLayout(new GridLayout());

    // Formatter panel
    formatterPane.setBorder(BorderFactory.createTitledBorder("Formatter"));

    // File panel
    filePane.setBorder(BorderFactory.createTitledBorder("Log file"));
    filePane.setLayout(new GridLayout(3, 1));

    // Button panel
    buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));

    // Path panel
    pathPane.setLayout(new BorderLayout());

    // Text field
    pathText.setPreferredSize(new Dimension(300, 20));
    if (owner.getLogFile() != null) {
      pathText.setText(owner.getLogFile().getPath());
      fileChooser.setCurrentDirectory(owner.getLogFile());
    } else {
      pathText.setText(System.getProperty("user.home") + File.separator + "ControlDeveloper.log");
      pathText.setEnabled(false);
    }

    // File chooser
    fileChooser.setAcceptAllFileFilterUsed(false);
    fileChooser.setFileFilter(logFileFilter);

    // Buttons
    okButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        owner.setLogVisible(guiLogCheck.isSelected());
        if (fileLogCheck.isSelected()) {
          File logFile = new File(pathText.getText());
          if (!logFile.getParentFile().exists()) {
            if (JOptionPane.showConfirmDialog(parent, "The path '" + logFile.getParent()
                + "' does not exist, Should it be created?") == JOptionPane.YES_OPTION)
              logFile.getParentFile().mkdirs();
            else
              return;
          }
          owner.setLogFile(logFile, false);
        } else
          owner.setLogFile(null);
        dispose();
      }
    });

    cancleButton.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent arg0) {
        dispose();
      }
    });

    browseButton.setEnabled(owner.getLogFile() != null);
    browseButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (fileChooser.showDialog(parent, "Select") == JFileChooser.APPROVE_OPTION)
          pathText.setText(fileChooser.getSelectedFile().getPath());
      }
    });

    // Check boxes
    guiLogCheck.setSelected(owner.logVisible());

    fileLogCheck.setSelected(owner.getLogFile() != null);
    fileLogCheck.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        pathText.setEnabled(fileLogCheck.isSelected());
        browseButton.setEnabled(fileLogCheck.isSelected());
        simpleButton.setEnabled(fileLogCheck.isSelected());
        xmlButton.setEnabled(fileLogCheck.isSelected());
      }
    });

    // Radio buttons
    formatterButtons.add(simpleButton);
    formatterButtons.add(xmlButton);

    xmlButton.setEnabled(owner.getLogFile() != null);
    simpleButton.setEnabled(owner.getLogFile() != null);
    simpleButton.setSelected(true);

    // Main dialog
    setLayout(new BorderLayout());
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    setResizable(false);
  }

  /**
   * Adds all the components to the window
   */
  private void buildWindow() {
    // GUI panel
    guiPane.add(guiLogCheck);

    // Path panel
    pathPane.add(new JLabel("Log file path"), BorderLayout.NORTH);
    pathPane.add(pathText, BorderLayout.CENTER);
    pathPane.add(browseButton, BorderLayout.EAST);

    // Formatter pane
    formatterPane.add(simpleButton);
    formatterPane.add(xmlButton);

    // File panel
    filePane.add(fileLogCheck);
    filePane.add(pathPane);
    filePane.add(formatterPane);

    // Button panel
    buttonPane.add(okButton);
    buttonPane.add(cancleButton);

    // Main dialog
    add(guiPane, BorderLayout.NORTH);
    add(filePane, BorderLayout.CENTER);
    add(buttonPane, BorderLayout.SOUTH);
    getRootPane().setDefaultButton(okButton);
    pack();
    setLocationRelativeTo(owner);
  }
}
