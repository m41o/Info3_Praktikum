package info3.praktikum.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;
import java.util.logging.XMLFormatter;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;

import info3.praktikum.interfaces.ICmdModelListener;
import info3.praktikum.interfaces.IIoTypeModelListener;
import info3.praktikum.model.Direction;
import info3.praktikum.model.Gear;
import info3.praktikum.model.Pause;

/**
 * Class representing the main window of the GUI
 *
 * @author Christoph Jabs, Markus Lochmann
 */
@SuppressWarnings("serial")
public class ControlDeveloper extends JFrame implements ICmdModelListener, IIoTypeModelListener {
  private static String stdName = "Control-Developer";

  // Config values
  private static final int HEIGHT = 1000;
  private static final int WIDTH = 600;
  private static final int EAST_WIDTH = 100;
  private static final int SOUTH_HEIGHT = 100;
  private static final int BUTTON_HEIGHT = 30;

  // Whether or not the log should be shown in GUI
  private boolean showLog = false;
  // Map of all the used action commands
  private HashMap<String, String> actionCommands = new HashMap<String, String>();
  // The controller for all buttons etc.
  private CombinedController combinedController = new CombinedController();

  private File logFile = null;
  private File txtFile = null;
  private FileHandler logFileHandler = null;

  private int wifiCardIndex;
  private ControlDeveloper parent = this;

  // Model
  protected ControlModelInterface model = ControlModelInterface.getInstance();

  // GUI-Components
  private JTable commandTable = new JTable();
  private JTextField currentCmdText = new JTextField();
  private JTextArea logText = new JTextArea();
  // Menu bar
  private MainMenuBar menuBar = new MainMenuBar(this);
  // Panels
  private JPanel southPane = new JPanel();
  private JTabbedPane textPane = new JTabbedPane();
  private JPanel eastPane = new JPanel();
  private JPanel configPane = new JPanel();
  private JPanel movePane = new JPanel();
  private JScrollPane logPane = new JScrollPane(logText);
  // Icons
  private ImageIcon upIcon =
      new ImageIcon(getClass().getResource("/ionicons_svg_md-arrow-dropup-circle.gif"));
  private ImageIcon downIcon =
      new ImageIcon(getClass().getResource("/ionicons_svg_md-arrow-dropdown-circle.gif"));
  private ImageIcon playIcon = new ImageIcon(getClass().getResource("/ionicons_svg_md-play.gif"));
  private ImageIcon stopIcon = new ImageIcon(getClass().getResource("/ionicons_svg_md-square.gif"));
  private ImageIcon addIcon =
      new ImageIcon(getClass().getResource("/ionicons_svg_md-add-circle.gif"));
  private ImageIcon removeIcon =
      new ImageIcon(getClass().getResource("/ionicons_svg_md-remove-circle.gif"));
  private ImageIcon clearIcon = new ImageIcon(getClass().getResource("/ionicons_svg_md-trash.gif"));
  private ImageIcon configIcon = new ImageIcon(getClass().getResource("/ionicons_svg_md-cog.gif"));
  // Buttons
  private JButton startStopButton = new JButton(playIcon);
  private JButton upButton = new JButton(upIcon);
  private JButton downButton = new JButton(downIcon);
  private JButton addButton = new JButton(addIcon);
  private JButton configButton = new JButton(configIcon);
  private JButton removeButton = new JButton(removeIcon);
  private JButton clearButton = new JButton(clearIcon);

  /**
   * @param args: Command line parameters.
   */
  public static void main(String[] args) {
    @SuppressWarnings("unused")
    ControlDeveloper cD = new ControlDeveloper();
  }

  /**
   * Generates a new ControlDeveloper window with a predefined name
   */
  public ControlDeveloper() {
    this(stdName);
  }

  /**
   * Generates a new ControlDeveloper window with a given name
   *
   * @param name The name of the new window
   */
  public ControlDeveloper(String name) {
    super(name);
    model.addCmdModelListener(this); // Subscribe for command changes
    testCommands();
    configureComponents();
    buildWindow();
    setVisible(true);
  }

  /**
   * Configures all the individual components
   */
  public void configureComponents() {
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    configureKeyBindings();

    // Buttons
    upButton.setPreferredSize(new Dimension((int) (EAST_WIDTH * 0.9), BUTTON_HEIGHT));
    upButton.setActionCommand(actionCommands.get("Up"));
    upButton.addActionListener(combinedController);
    downButton.setPreferredSize(new Dimension((int) (EAST_WIDTH * 0.9), BUTTON_HEIGHT));
    downButton.setActionCommand(actionCommands.get("Down"));
    downButton.addActionListener(combinedController);
    addButton.setPreferredSize(new Dimension((int) (EAST_WIDTH * 0.9), BUTTON_HEIGHT));
    addButton.setActionCommand(actionCommands.get("Add"));
    addButton.addActionListener(combinedController);
    configButton.setPreferredSize(new Dimension((int) (EAST_WIDTH * 0.9), BUTTON_HEIGHT));
    configButton.setActionCommand(actionCommands.get("Config"));
    configButton.addActionListener(combinedController);
    removeButton.setPreferredSize(new Dimension((int) (EAST_WIDTH * 0.9), BUTTON_HEIGHT));
    removeButton.setActionCommand(actionCommands.get("Remove"));
    removeButton.addActionListener(combinedController);
    clearButton.setPreferredSize(new Dimension((int) (EAST_WIDTH * 0.9), BUTTON_HEIGHT));
    clearButton.setActionCommand(actionCommands.get("Clear"));
    clearButton.addActionListener(combinedController);
    startStopButton.setPreferredSize(new Dimension(EAST_WIDTH, SOUTH_HEIGHT));
    startStopButton.setActionCommand(actionCommands.get("Start/Stop"));
    startStopButton.addActionListener(combinedController);
    startStopButton.setEnabled(model.getIoType() != null);
    model.addIoTypeModelListener(this);

    // Table
    commandTable.setModel(model);
    commandTable.getColumnModel().getColumn(0).setMaxWidth(60);
    commandTable.getColumnModel().getColumn(0).setPreferredWidth(60);
    commandTable.getColumnModel().getColumn(1).setMaxWidth(250);
    commandTable.getColumnModel().getColumn(1).setPreferredWidth(150);
    commandTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
    commandTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    commandTable.addMouseListener(combinedController);

    // Text fields
    currentCmdText.setEditable(false);
    logText.setEditable(false);
    model.addLogHandler(new TextAreaHandler(logText, new SimpleFormatter()));

    // Panels
    // South panel
    southPane.setLayout(new BorderLayout());
    southPane.setPreferredSize(new Dimension(HEIGHT, SOUTH_HEIGHT));

    // East panel
    eastPane.setPreferredSize(new Dimension(EAST_WIDTH, WIDTH - SOUTH_HEIGHT));
    eastPane.setLayout(new GridLayout(2, 1));

    // Config panel
    configPane.setLayout(new GridBagLayout());

    // Move panel
    movePane.setLayout(new GridBagLayout());
  }

  /**
   * Adds all the components to the window
   */
  private void buildWindow() {
    GridBagConstraints constraints = new GridBagConstraints();

    // Move panel
    constraints.insets = new Insets(4, 4, 4, 4);
    movePane.add(upButton, constraints);
    constraints.gridy = 1;
    movePane.add(downButton, constraints);

    // Config panel
    constraints.gridy = 0;
    configPane.add(addButton, constraints);
    constraints.gridy = 1;
    configPane.add(configButton, constraints);
    constraints.gridy = 2;
    configPane.add(removeButton, constraints);
    constraints.gridy = 3;
    configPane.add(clearButton, constraints);

    // East panel
    eastPane.add(movePane, BorderLayout.NORTH);
    eastPane.add(configPane, BorderLayout.SOUTH);

    // South center panel
    textPane.add("Current Command", currentCmdText);
    if (showLog)
      textPane.add("Log", logPane);

    // South panel
    southPane.add(startStopButton, BorderLayout.EAST);
    southPane.add(textPane, BorderLayout.CENTER);

    // Main window
    setJMenuBar(menuBar);
    add(new JScrollPane(commandTable), BorderLayout.CENTER);
    add(eastPane, BorderLayout.EAST);
    add(southPane, BorderLayout.SOUTH);
    setSize(HEIGHT, WIDTH);
    setLocationRelativeTo(null);
  }

  /**
   * Set up keybindings and configure action command hash map
   */
  private void configureKeyBindings() {
    // Set action commands and therfore hot keys
    actionCommands.put("Start/Stop", "s");
    actionCommands.put("Up", "k");
    actionCommands.put("Down", "j");
    actionCommands.put("Add", "a");
    actionCommands.put("Remove", "" + ((char) 127)); // Delete
    actionCommands.put("Config", "c");
    actionCommands.put("Clear", "x");
    // Key bindings
    InputMap inputMap = commandTable.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
    ActionMap actionMap = commandTable.getActionMap();

    inputMap.put(KeyStroke.getKeyStroke('s'), "Start/Stop");
    actionMap.put("Start/Stop", combinedController);
    inputMap.put(KeyStroke.getKeyStroke('k'), "Up");
    actionMap.put("Up", combinedController);
    inputMap.put(KeyStroke.getKeyStroke('j'), "Down");
    actionMap.put("Down", combinedController);
    inputMap.put(KeyStroke.getKeyStroke('a'), "Add");
    actionMap.put("Add", combinedController);
    inputMap.put(KeyStroke.getKeyStroke((char) 127), "Remove"); // Delete
    actionMap.put("Remove", combinedController);
    inputMap.put(KeyStroke.getKeyStroke('c'), "Config");
    actionMap.put("Config", combinedController);
    inputMap.put(KeyStroke.getKeyStroke('x'), "Clear");
    actionMap.put("Clear", combinedController);
  }

  /**
   * fills commands and commandList with test commands
   */
  public void testCommands() {
    model.add(new Direction(45));
    model.add(new Gear(500, 2.5));
    model.add(new Pause(5.0));
  }

  /**
   * prints commands and commandList to screen
   */
  public void printCommands() {
    System.out.println("Printing commandList:");
    System.out.println(model);
  }

  /**
   * @return the eastWidth
   */
  public int getEastWidth() {
    return EAST_WIDTH;
  }

  /**
   * @return the southHeight
   */
  public int getSouthHeight() {
    return SOUTH_HEIGHT;
  }

  /**
   * @return the logFile
   */
  public File getLogFile() {
    return logFile;
  }

  /**
   * Set the log file, choosing whether it should be XML formatted or not
   *
   * @param logFile the logFile to set
   * @param xmlFormat whether the log file should be formatted as XML or not
   */
  public void setLogFile(File logFile, boolean xmlFormat) {
    this.logFile = logFile;
    model.removeLogHandler(logFileHandler);
    if (logFile != null) {
      try {
        logFileHandler = new FileHandler(logFile.getPath());
      } catch (SecurityException | IOException e) {
        e.printStackTrace();
      }
      if (xmlFormat)
        logFileHandler.setFormatter(new XMLFormatter());
      else
        logFileHandler.setFormatter(new SimpleFormatter());
      model.addLogHandler(logFileHandler);
    }
  }

  /**
   * Set the log file with simple formatting
   *
   * @param logFile the logFile to set
   */
  public void setLogFile(File logFile) {
    setLogFile(logFile, false);
  }

  /**
   * @return the txtFile
   */
  public File getTxtFile() {
    return txtFile;
  }

  /**
   * @param txtFile the txtFile to set
   */
  public void setTxtFile(File txtFile) {
    this.txtFile = txtFile;
  }

  /**
   * @return the wifiCardIndex
   */
  public int getWifiCardIndex() {
    return wifiCardIndex;
  }

  /**
   * @param wifiCardIndex the wifiCardIndex to set
   */
  public void setWifiCardIndex(int wifiCardIndex) {
    this.wifiCardIndex = wifiCardIndex;
  }

  /**
   * @return the showLog
   */
  public boolean logVisible() {
    return showLog;
  }

  /**
   * @param showLog the showLog to set
   */
  public void setLogVisible(boolean showLog) {
    this.showLog = showLog;
    if (showLog)
      textPane.add("Log", logPane);
    else
      textPane.remove(logPane);
  }

  /**
   * @return the width
   */
  public int getFrameWidth() {
    return HEIGHT;
  }

  /**
   * @return the height
   */
  public int getFrameHeight() {
    return WIDTH;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void commandChanged() {
    try {
      currentCmdText.setText(model.getCommand().toString());
    } catch (NullPointerException e) {
      currentCmdText.setText(null);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void ioTypeChanged() {
    startStopButton.setEnabled(model.getIoType() != null);
  }

  /**
   * Class functioning as controller for all components
   */
  class CombinedController extends AbstractAction implements MouseListener {
    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      if (e.getActionCommand().equals(actionCommands.get("Up"))) {
        int selectedRow = commandTable.getSelectedRow();
        int selectedColumn = commandTable.getSelectedColumn();
        if (model.moveUp(selectedRow))
          commandTable.changeSelection(selectedRow - 1, selectedColumn, false, false);
      } else if (e.getActionCommand().equals(actionCommands.get("Down"))) {
        int selectedRow = commandTable.getSelectedRow();
        int selectedColumn = commandTable.getSelectedColumn();
        if (model.moveDown(selectedRow))
          commandTable.changeSelection(selectedRow + 1, selectedColumn, false, false);
      } else if (e.getActionCommand().equals(actionCommands.get("Add"))) {
        ConfigMenu configMenu = new ConfigMenu(parent, "Add Command");
        configMenu.setVisible(true);
      } else if (e.getActionCommand().equals(actionCommands.get("Config"))) {
        if (commandTable.getSelectedRow() != -1) {
          ConfigMenu configMenu =
              new ConfigMenu(parent, "Configure Command", model.get(commandTable.getSelectedRow()));
          configMenu.setVisible(true);
        }
      } else if (e.getActionCommand().equals(actionCommands.get("Remove")))
        model.remove(commandTable.getSelectedRow());
      else if (e.getActionCommand().equals(actionCommands.get("Clear")))
        model.clear();
      else if (e.getActionCommand().equals(actionCommands.get("Start/Stop"))) {
        if (model.getIoType() == null)
          JOptionPane.showMessageDialog(parent,
              "There is no target for sending the commands selected. "
                  + "Please select either a Wifi card or a file in 'Options > Wifi Settings'",
              "No target selected", JOptionPane.WARNING_MESSAGE);
        else if (model.toggleStatus())
          startStopButton.setIcon(stopIcon);
        else
          startStopButton.setIcon(playIcon);
      }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
      if (e.getSource() == commandTable)
        if (e.getClickCount() == 2)
          actionPerformed(new ActionEvent(e.getSource(), e.getID(), actionCommands.get("Config")));
    }

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}
  }

  /**
   * Handler to display log in text area
   */
  class TextAreaHandler extends Handler {
    private final JTextArea textArea;

    /**
     * Create handler
     *
     * @param textArea The text are in which the log should be displayed
     * @param formatter The formatter to use
     */
    TextAreaHandler(JTextArea textArea, Formatter formatter) {
      this.textArea = textArea;
      setFormatter(formatter);
    }

    @Override
    public void publish(LogRecord record) {
      if (isLoggable(record))
        textArea.append(getFormatter().format(record));
    }

    @Override
    public void flush() {}

    @Override
    public void close() throws SecurityException {}
  }
}
