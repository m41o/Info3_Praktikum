package info3.praktikum.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import hsrt.mec.controldeveloper.core.com.WiFiCard;
import hsrt.mec.controldeveloper.io.TextFile;
import hsrt.mec.controldeveloper.io.WiFi;

/**
 * WifiMenu dialog for Wifi settings
 *
 * @author Markus Lochmann
 *
 */
public class WifiMenu extends JDialog {
  // Java Swing components
  private JButton okButton, cancelButton, browseButton;
  private JRadioButton wifiCheck, fileCheck;
  private JComboBox<String> wifiNames;
  private JPanel southPanel, centerPanel, wifiPanel, filePanel;
  private JTextField filePath;

  // File chooser and file extension filter
  private JFileChooser fileChooser = new JFileChooser();
  private FileNameExtensionFilter txtFileFilter = new FileNameExtensionFilter("Text files", "txt");

  /**
   * Variable same as this that can be passed to ActionListeners
   */
  private JDialog me = this;
  
  /**
   * Parent of this dialog (main window of ControlDeveloper)
   */
  private ControlDeveloper parent;
  
  /**
   * Array of wifi cards available to the system
   */
  private WiFiCard[] wifiCards;
  
  /**
   * True if a wifi interface is available
   */
  private boolean hasWifi;

  /**
   * Construct a new WifiMenu dialog
   *
   * @param parent Parent container
   */
  public WifiMenu(ControlDeveloper parent) {
    super(parent, "Wifi Settings", true);
    this.parent = parent;
    wifiCards = parent.model.getWiFiCards();
    hasWifi = (wifiCards.length != 0);
    buildDialog();
  }

  /**
   * Builds the WifiMenu dialog
   */
  private void buildDialog() {
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    setResizable(false);
    setLayout(new BorderLayout());
    configureSouthPanel();
    configureCenterPanel();
    pack();
    setLocationRelativeTo(parent);
    setVisible(true);
  }

  /**
   * Configures the buttons on south panel
   */
  private void configureSouthPanel() {
    okButton = new JButton("OK");
    cancelButton = new JButton("Cancel");

    okButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (wifiCheck.isSelected()) {
          int i = wifiNames.getSelectedIndex();
          parent.model.setIoType(new WiFi(wifiCards[i]));
          parent.setWifiCardIndex(i);
        } else if (fileCheck.isSelected()) {
          File file = new File(filePath.getText());
          if (!file.getParentFile().exists()) {
            if (JOptionPane.showConfirmDialog(parent, "The path '" + file.getParent()
                + "' does not exist, Should it be created?") == JOptionPane.YES_OPTION)
              file.getParentFile().mkdirs();
            else
              return;
          }
          parent.setTxtFile(file);
          parent.model.setIoType(new TextFile(file, false));
        } else
          parent.model.setIoType(null);

        dispose();
      }
    });

    cancelButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        dispose();
      }
    });

    southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    southPanel.add(okButton);
    southPanel.add(cancelButton);
    add(southPanel, BorderLayout.SOUTH);
    getRootPane().setDefaultButton(okButton);
  }

  /**
   * Configure centerPanel
   */
  private void configureCenterPanel() {
    configureWifiPanel();
    configureFilePanel();

    centerPanel = new JPanel(new BorderLayout());
    centerPanel.add(wifiPanel, BorderLayout.NORTH);
    centerPanel.add(filePanel, BorderLayout.CENTER);
    add(centerPanel, BorderLayout.CENTER);
  }

  /**
   * Configure radio button, text field and button on filePanel
   */
  private void configureFilePanel() {
    fileCheck = new JRadioButton("Write Commands to File");
    fileCheck.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        filePath.setEnabled(fileCheck.isSelected());
        browseButton.setEnabled(fileCheck.isSelected());
        if (fileCheck.isSelected()) {
          wifiCheck.setSelected(false);
          wifiNames.setEnabled(false);
        }
      }
    });

    browseButton = new JButton("...");
    browseButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (fileChooser.showDialog(me, "Select File") == JFileChooser.APPROVE_OPTION)
          filePath.setText(fileChooser.getSelectedFile().getPath());
      }
    });

    fileChooser.setAcceptAllFileFilterUsed(false);
    fileChooser.setFileFilter(txtFileFilter);

    filePath = new JTextField();

    if (parent.model.getIoType() instanceof TextFile) {
      fileCheck.setSelected(true);
      filePath.setText(parent.getTxtFile().getPath());
    } else
      filePath.setText(System.getProperty("user.home") + File.separator + "ControlProcess.txt");

    filePath.setEnabled(fileCheck.isSelected());
    browseButton.setEnabled(fileCheck.isSelected());

    filePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    filePanel.setBorder(BorderFactory.createTitledBorder("File"));
    filePanel.add(fileCheck);
    filePanel.add(filePath);
    filePanel.add(browseButton);
  }

  /**
   * Configure CheckBox and ComboBox
   */
  private void configureWifiPanel() {
    wifiCheck = new JRadioButton("Use WiFi");
    wifiCheck.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (!hasWifi) {
          JOptionPane.showMessageDialog(me, "There are no WiFi-Cards available!", "Wifi Warning",
              JOptionPane.WARNING_MESSAGE);
          wifiCheck.setSelected(false);
        }
        wifiNames.setEnabled(wifiCheck.isSelected());
        if (wifiCheck.isSelected()) {
          fileCheck.setSelected(false);
          filePath.setEnabled(false);
          browseButton.setEnabled(false);
        }
      }
    });

    String[] names = {"..."};

    if (hasWifi) {
      names = new String[wifiCards.length];
      for (int i = 0; i < wifiCards.length; i++)
        names[i] = wifiCards[i].getDisplayName();
    }

    wifiNames = new JComboBox<String>(names);
    wifiNames.setEditable(false);

    if (parent.model.getIoType() instanceof WiFi) {
      wifiCheck.setSelected(true);
      wifiNames.setSelectedIndex(parent.getWifiCardIndex());
    }
    wifiNames.setEnabled(wifiCheck.isSelected());

    wifiPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    wifiPanel.setBorder(BorderFactory.createTitledBorder("WiFi"));
    wifiPanel.add(wifiCheck);
    wifiPanel.add(wifiNames);
  }
}
