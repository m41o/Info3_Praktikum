package info3.praktikum.view;

import java.awt.Insets;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

/**
 * About window of ControlDeveloper showing content of about.txt
 *
 * @author Markus Lochmann
 * @author Christoph Jabs
 */
public class AboutMenu extends JDialog {
  private static final String PATH = "about.html";
  private static final int WIDTH = 600;
  private static final int HEIGHT = 300;
  private static final int MARGIN = 15;

  private JTextPane text = null;
  private JScrollPane scroll = null;

  private String srcCode = "";

  /**
   * Construct about window
   *
   * @param parent the parent frame of the about menu
   * @param title The title of the frame
   */
  public AboutMenu(JFrame parent, String title) {
    super(parent, title, false);
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    setSize(WIDTH, HEIGHT);
    setLocationRelativeTo(parent);

    readFile(PATH);
    text = new JTextPane();
    text.setContentType("text/html");
    text.setText(srcCode);
    text.setEditable(false);
    text.setMargin(new Insets(MARGIN, MARGIN, MARGIN, MARGIN));
    text.setCaretPosition(0);
    scroll = new JScrollPane(text);

    add(scroll);
    setVisible(true);
  }

  /**
   * Reads the contents of specified .html file into srcCode
   * @param path The Path to the file to read from
   */
  private void readFile(String path) {

    try {
      BufferedReader reader = new BufferedReader(new FileReader(path));
      String line;

      while ((line = reader.readLine()) != null) {
        srcCode += line;
      }
      reader.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Creates a new about menu window.
   * To be called in the actionPerfromed method of the menuItem
   * 
   * @param parent Parent frame
   * @param title Title of the window
   */
  public static void createAboutMenu(JFrame parent, String title) {
    new AboutMenu(parent, title);
  }
}
