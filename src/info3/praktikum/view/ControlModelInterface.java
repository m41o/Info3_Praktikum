package info3.praktikum.view;

import java.util.Vector;
import java.util.logging.Level;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import hsrt.mec.controldeveloper.core.com.command.ICommand;
import hsrt.mec.controldeveloper.io.IOType;

import info3.praktikum.interfaces.ICmdModel;
import info3.praktikum.interfaces.ICmdModelListener;
import info3.praktikum.interfaces.IIoTypeModel;
import info3.praktikum.interfaces.IIoTypeModelListener;
import info3.praktikum.model.Command;
import info3.praktikum.model.ControlModel;

/**
 * Extended Control model that implements intefaces for connecting to the GUI
 *
 * @author Christoph Jabs
 */
public class ControlModelInterface extends ControlModel implements TableModel, ICmdModel, IIoTypeModel {
  /**
   * Only instance of ControlTableModel (Singleton)
   */
  private static ControlModelInterface instance = null;

  /**
   * Vector of subscribed tables listening to this model
   */
  private Vector<TableModelListener> tableModelListeners = new Vector<TableModelListener>();

  /**
   * Vector of subscribers listening to this command model
   */
  private Vector<ICmdModelListener> cmdModelListeners = new Vector<ICmdModelListener>();

  /**
   * Vector of subscribers listening to this ioType model
   */
  private Vector<IIoTypeModelListener> ioTypeModelListeners = new Vector<IIoTypeModelListener>();

  /**
   * The names of the columns if model is displayed as table
   */
  private String[] columnNames = {"No.", "Command", "Configuration"};

  /**
   * Private default constructor for ControlModel
   */
  private ControlModelInterface() {}

  /**
   * Get instance of ControlModel
   *
   * @return instance of ControlModel
   */
  public static ControlModelInterface getInstance() {
    if (instance == null)
      instance = new ControlModelInterface();
    instance.log(Level.INFO, "Requested instance");

    return instance;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void clear() {
    super.clear();
    notifyTableListeners();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean add(ICommand newCommand) {
    boolean returnVal = super.add(newCommand);
    notifyTableListeners();
    return returnVal;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean moveDown(int index, int n) {
    boolean returnVal = super.moveDown(index, n);
    notifyTableListeners();
    return returnVal;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean moveDown(int index) {
    boolean returnVal = super.moveDown(index);
    notifyTableListeners();
    return returnVal;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean moveUp(int index, int n) {
    boolean returnVal = super.moveUp(index, n);
    notifyTableListeners();
    return returnVal;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean moveUp(int index) {
    boolean returnVal = super.moveUp(index);
    notifyTableListeners();
    return returnVal;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean remove(int index) {
    boolean returnVal = super.remove(index);
    notifyTableListeners();
    return returnVal;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean replace(ICommand oldCmd, ICommand newCmd) {
    boolean returnVal = super.replace(oldCmd, newCmd);
    notifyTableListeners();
    return returnVal;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setIoType(IOType ioType) {
    super.setIoType(ioType);
    notifyIoTypeListeners();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void commandPerformed(ICommand command) {
    super.commandPerformed(command);
    notifyCmdListeners();
  }

  /**
   * {@inheritDoc}
   */
  public boolean toggleStatus() {
    boolean retVal = super.toggleStatus();
    notifyCmdListeners();
    return retVal;
  }

  // ListModel methods
  /**
   * {@inheritDoc}
   */
  @Override
  public void addTableModelListener(TableModelListener l) {
    tableModelListeners.add(l);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void removeTableModelListener(TableModelListener l) {
    tableModelListeners.remove(l);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Class<?> getColumnClass(int columnIndex) {
    switch (columnIndex) {
      case 0:
        return Integer.class;
      case 1:
        return String.class;
      case 2:
        return String.class;
      default:
        return null;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getColumnCount() {
    return columnNames.length;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getRowCount() {
    return length();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getColumnName(int columnIndex) {
    return columnNames[columnIndex];
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    switch (columnIndex) {
      case 0:
        return rowIndex;
      case 1:
        return get(rowIndex).getName();
      case 2:
        return ((Command) get(rowIndex)).parseConfig();
      default:
        return null;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    return false;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setValueAt(Object aValue, int rowIndex, int columnIndex) {}

  /**
   * Notify all subscribed table listeners that table has changed
   */
  private void notifyTableListeners() {
    for (TableModelListener tableListener : tableModelListeners)
      tableListener.tableChanged(new TableModelEvent(this));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void addCmdModelListener(ICmdModelListener listener) {
    cmdModelListeners.add(listener);
    log(Level.INFO, "Added CmdModelListener");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void removeCmdModelListener(ICmdModelListener listener) {
    cmdModelListeners.remove(listener);
    log(Level.INFO, "Removed CmdModelListener");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Command getCommand() {
    return (Command) currentCommand;
  }

  /**
   * Notify listeners, that command has changed
   */
  private void notifyCmdListeners() {
    for (ICmdModelListener cmdListener : cmdModelListeners)
      cmdListener.commandChanged();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void addIoTypeModelListener(IIoTypeModelListener listener) {
    ioTypeModelListeners.add(listener);
    log(Level.INFO, "Added IoTypeModelListener");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void removeIoTypeModelListener(IIoTypeModelListener listener) {
    ioTypeModelListeners.remove(listener);
    log(Level.INFO, "Removed IoTypeModelListener");
  }

  /**
   * Notify listeners, that ioType has changed
   */
  private void notifyIoTypeListeners() {
    for (IIoTypeModelListener ioTypeListener : ioTypeModelListeners)
      ioTypeListener.ioTypeChanged();
  }
}
