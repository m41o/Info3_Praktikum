package info3.praktikum.interfaces;

/**
 * Interface to inform listeners about ioType changes
 *
 * @author Christoph Jabs (christoph@jabs-family.de)
 */
public interface IIoTypeModelListener {
  /**
   * Method that is called if the ioType has changed
   */
  public void ioTypeChanged();
}
