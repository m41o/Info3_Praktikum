package info3.praktikum.interfaces;

import hsrt.mec.controldeveloper.io.IOType;

/**
 * Interface for keeping a listener in sync with a ioType model
 *
 * @author Christoph Jabs (christoph@jabs-family.de)
 */
public interface IIoTypeModel {
  /**
   * Add a new listener to the model
   *
   * @param listener The new listener
   */
  public void addIoTypeModelListener(IIoTypeModelListener listener);

  /**
   * Remove a listener from the model
   *
   * @param listener The listener to remove
   */
  public void removeIoTypeModelListener(IIoTypeModelListener listener);

  /**
   * Get the current ioType of the model
   *
   * @return The command
   */
  public IOType getIoType();
}
