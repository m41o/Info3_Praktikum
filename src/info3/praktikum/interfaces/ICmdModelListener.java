package info3.praktikum.interfaces;

/**
 * Interface to inform listeners about command changes
 *
 * @author Christoph Jabs (christoph@jabs-family.de)
 */
public interface ICmdModelListener {
  /**
   * method that is called if the command has changed
   */
  public void commandChanged();
}
