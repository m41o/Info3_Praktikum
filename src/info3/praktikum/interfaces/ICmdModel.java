package info3.praktikum.interfaces;

import info3.praktikum.model.Command;

/**
 * Interface for keeping a listener in sync with a command model
 *
 * @author Christoph Jabs (christoph@jabs-family.de)
 */
public interface ICmdModel {
  /**
   * Add a new listener to the model
   *
   * @param listener The new listener
   */
  public void addCmdModelListener(ICmdModelListener listener);

  /**
   * Remove a listener from the model
   *
   * @param listener The listener to remove
   */
  public void removeCmdModelListener(ICmdModelListener listener);

  /**
   * Get the current command of the model
   *
   * @return The command
   */
  public Command getCommand();
}
