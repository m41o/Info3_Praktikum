package info3.praktikum.model;

import hsrt.mec.controldeveloper.core.com.command.ICommand;

public class CommandType {
  
  /**
   * Constants that identify CommandTypes
   */
  public static final int DIRECTION = 0;
  public static final int GEAR      = 1;
  public static final int PAUSE     = 2;
  
  /**
   * The name of the given CommandType
   */
  private String name;

  /**
   * Creates a command type with a given name
   *
   * @param name The name to be set
   */
  public CommandType(String name) {
    this.name = name;
  }

  /**
   * Creates a new instance of the given CommandType
   * 
   * @param name Name of the class to be instantiated
   * @return The newly created instance of the command type
   */
  public static ICommand createInstance(String name) {
    // Prepend package name if none is given
    String fullName = name;
    if (name.indexOf('.') == -1)
      fullName = Command.class.getPackage().getName() + "." + name;

    ICommand newInstance;
    Object tmpObj;
    try {
      Class<?> type = Class.forName(fullName);
      tmpObj = type.newInstance();
    } catch (Exception e) {
      System.out.println(e);
      tmpObj = null;
    }
    if (tmpObj instanceof ICommand)
      newInstance = (ICommand) tmpObj;
    else
      newInstance = null;
    return newInstance;
  }

  /**
   * Creates a new instance of the given CommandType
   *
   * @return The newly created instance of the command type
   */
  public ICommand createInstance() {
    return createInstance(this.name);
  }
  
  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return "Type: " + name;
  }

  public static void main(String[] args) {
    CommandType ct = new CommandType("Gear");

    ICommand ic = ct.createInstance();

    System.out.println(((Command)ic).serialize(";"));
    System.out.println(ct);
    System.out.println(ic);
  }
}
