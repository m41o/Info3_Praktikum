package info3.praktikum.model;

import hsrt.mec.controldeveloper.core.com.command.IGear;

/**
 * Command that moves with set speed for set duration
 *
 * @author Markus Lochmann
 */
public class Gear extends Command implements IGear {
  /**
   * The name of the command
   */
  public static final String NAME;
  static {
    NAME = Gear.class.getSimpleName();
  }

  /**
   * Maximum speed
   */
  private static final int MAX_SPEED = 100;

  /**
   * Minimum speed
   */
  private static final int MIN_SPEED = -100;
  
  /**
   * Key for first parameter
   */
  public static final String FIRST_PARAM = "Duration";
  
  /**
   * Key for second parameter
   */
  public static final String SECOND_PARAM = "Speed";
  
  /**
   * Speed to move at [-100, 1000]
   */
  private int speed = 0;

  /**
   * Duration to move for in seconds
   */
  private double duration = 0.0;

  /**
   * Construct Command called "Gear" and set standard speed of 50 and standard duration of 100
   */
  public Gear() {
    super(NAME);
    params = new String[] {FIRST_PARAM, SECOND_PARAM};
    index = CommandType.GEAR;
    setSpeed(0);
    setDuration(0.0);
  }

  /**
   * Construct Command called "Gear" and set speed and duration
   *
   * @param speed the speed to set
   * @param duration the duration to set
   */
  public Gear(int speed, double duration) {
    this();
    setSpeed(speed);
    setDuration(duration);
  }

  /**
   * Get duration to move for
   *
   * @see hsrt.mec.controldeveloper.core.com.command.IGear#getDuration()
   *
   * @return duration
   */
  @Override
  public double getDuration() {
    return duration;
  }

  /**
   * Get speed to move at
   *
   * @see hsrt.mec.controldeveloper.core.com.command.IGear#getSpeed()
   *
   * @return speed
   */
  @Override
  public int getSpeed() {
    return speed;
  }

  /**
   * Set speed to move at
   *
   * @param speed the speed to set [-100, 1000]
   */
  public void setSpeed(int speed) {
    if (speed >= MIN_SPEED && speed <= MAX_SPEED)
      this.speed = speed;
    else if (speed < MIN_SPEED)
      this.speed = MIN_SPEED;
    else
      this.speed = MAX_SPEED;
  }

  /**
   * Set duration to move for
   *
   * @param duration the duration to set
   */
  public void setDuration(double duration) {
    if (duration >= 0)
      this.duration = duration;
    else
      this.duration = 0.0;
  }

  /**
   * Get readable info of Gear
   *
   * @return String representation of Gear
   */
  @Override
  public String toString() {
    String str = getName();

    if (speed < 0)
      str += ": " + -speed + "% power backwards";
    else
      str += ": " + speed + "% power forwards";

    return str + " for " + duration + " seconds";
  }

  @Override
  public String serialize(String separator) {
    return getName() + separator + getDuration() + separator + getSpeed();
  }

  /**
   * Set duration to move for
   *
   * @param p the duration to set
   */
  @Override
  public void setFirstParam(Number p) {
    setDuration(p.doubleValue());
  }

  /**
   * Set speed to move at
   *
   * @param p the speed to set [-100, 1000]
   */
  @Override
  public void setSecondParam(Number p) {
    setSpeed(p.intValue());
  }

  @Override
  public Number getFirstParam() {
    return new Double(getDuration());
  }

  @Override
  public Number getSecondParam() {
    return new Integer(getSpeed());
  }

  @Override
  public String parseConfig() {
    return speed + "cm/s " + duration + "s";
  }

  @Override
  public Number getFirstParamMax() {
    return null;
  }

  @Override
  public Number getFirstParamMin() {
    return new Double(0.0);
  }

  @Override
  public Number getSecondParamMax() {
    return new Integer(MAX_SPEED);
  }

  @Override
  public Number getSecondParamMin() {
    return new Integer(MIN_SPEED);
  }
}
