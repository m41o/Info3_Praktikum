package info3.praktikum.model;

import java.util.Iterator;
import java.util.Vector;

import hsrt.mec.controldeveloper.core.com.command.ICommand;

/**
 * Linked list containing commands.
 *
 * @author Christoph Jabs
 */

public class CommandList implements Iterable<ICommand> {
  /**
   * Return value if Command is not found in CommandList
   */
  public static final int NOT_FOUND = -1;

  /**
   * Root of the CommandList
   */
  private Element root = null;

  /**
   * Length of the CommandList
   */
  private int length = 0;

  /**
   * Single element of the linked list.
   */
  private class Element {
    /**
     * Reference to the element after the current one.
     */
    private Element next = null;

    /**
     * Reference to the element previous to the current one.
     */
    private Element prev = null;

    /**
     * The command stored in the list element.
     */
    private ICommand element;

    /**
     * Construct element holding a command
     *
     * @param element the command to store
     */
    public Element(ICommand element) {
      this.element = element;
    }

    /**
     * Construct element behind prev, holding a command
     *
     * @param element the command to store
     * @param prev the previous element in the list
     */
    public Element(ICommand element, Element prev) {
      this.prev = prev;
      this.element = element;
    }

    /**
     * Get the command stored in the element
     *
     * @return the element
     */
    public ICommand getElement() {
      return element;
    }

    /**
     * Set the command stored in the element
     * @param element Command to be stored
     */
    public void setElement(ICommand element) {
      this.element = element;
    }
    
    /**
     * @param obj object to compare to
     * @return if compared to a command or another element, the commands are compared, not the
     *         elements
     */
    @Override
    public boolean equals(Object obj) {
      if (obj instanceof Element) // If compared to other element, compare commands
        return getElement().equals(((Element) obj).getElement());
      if (obj instanceof ICommand) // If compared to a command, compare commands
        return getElement().equals((ICommand) obj);
      return super.equals(obj);
    }

    /**
     * @return the name of the command stored in the element
     */
    @Override
    public String toString() {
      return getElement().toString();
    }
  }

  /**
   * Iterator for using for each loops with CommandList.
   */
  private class CommandIterator implements Iterator<ICommand> {
    /**
     * The index the iterator is currently at.
     */
    private int index = 0; // Index that the iterator stands on

    /**
     * @return true if CommandList contains a next command.
     */
    @Override
    public boolean hasNext() {
      if (index < length)
        return true;
      return false;
    }

    /**
     * @return the next Command in the list
     */
    @Override
    public ICommand next() {
      return get(index++);
    }
  }

  /**
   * @return an iterator for for each loops
   */
  @Override
  public Iterator<ICommand> iterator() {
    return new CommandIterator();
  }


  @Override
  public boolean equals(Object obj) {
    if (obj instanceof CommandList) { // If comparing two command lists, compare each element
      CommandList list2 = (CommandList) obj;
      if (length != list2.length) // Unequal length
        return false;
      for (int i = 0; i < length; i++)
        if (!get(i).equals(list2.get(i))) // Unequal elements
          return false;
      return true;
    }
    return super.equals(obj);
  }

  @Override
  public String toString() {
    if (length == 0)
      return "| Empty List |";

    String readableValue = "|";
    for (ICommand iC : this)
      readableValue += " " + iC.toString() + " |";
    return readableValue;
  }

  /**
   * @return the length
   */
  public int length() {
    return length;
  }


  /**
   * Find element with given index in the list
   *
   * @param index the index of the element that should be found
   * @return the element with the given index (null if index is out of bounds)
   */
  private Element find(int index) {
    if (index >= length || index < 0)
      return null; // Index out of bounds

    Element tmpElement = root;
    for (int i = 0; i < index; i++)
      tmpElement = tmpElement.next;
    return tmpElement;
  }

  /**
   * Check if the list includes a specific element
   *
   * @param e the element to search for
   * @return the index of the command if it's found, CommandList.NOT_FOUND if it's not found
   */
  @SuppressWarnings("unused")
  private int includes(Element e) {
    for (int i = 0; i < length; i++)
      if (find(i).equals(e))
        return i;
    return NOT_FOUND;
  }

  /**
   * Check if the list includes a specific command
   *
   * @param c the command to search for
   * @return the index of the command if it's found, CommandList.NOT_FOUND if it's not found
   */
  public int includes(ICommand c) {
    for (int i = 0; i < length; i++)
      if (get(i).equals(c))
        return i;
    return NOT_FOUND;
  }

  /**
   * Replaces a given ICommand with another one
   * 
   * @param oldCmd ICommand that is to be replaced
   * @param newCmd New ICommand
   * @return false, if command was not found; true otherwise
   */
  public boolean replace(ICommand oldCmd, ICommand newCmd) {
    int i = includes(oldCmd);
    if (i != NOT_FOUND) {
      find(i).setElement(newCmd);
      return true;
    }
    else
      return false;
  }
  
  /**
   * Add a command to the end of the list
   *
   * @param newCommand the command to add to the list
   * @return true if the action was successfull
   */
  public boolean add(ICommand newCommand) {
    if (root == null) {
      root = new Element(newCommand);
      length++;
      return true;
    }

    Element tmpElement = find(length - 1); // Find the last element
    tmpElement.next = new Element(newCommand, tmpElement);
    length++;
    return true;
  }

  /**
   * Remove element at given index from the list
   *
   * @param index the index of the element that should be removed
   * @return true if the element was removed, false if the index was out of bound
   */
  public boolean remove(int index) {
    if (index >= length || index < 0)
      return false; // Index out of bounds

    Element tmpElement = find(index); // Find the element that should be deleted

    if (tmpElement.prev == null) { // First element
      root = tmpElement.next;
      if (tmpElement.next != null) // Next element exists
        tmpElement.next.prev = null;
    } else { // Not first element
      tmpElement.prev.next = tmpElement.next; // Connect next and previous elements to each other
      if (tmpElement.next != null) // Next element exists
        tmpElement.next.prev = tmpElement.prev;
    }
    length--;
    return true;
  }

  /**
   * Get command stored at given index
   *
   * @param index the index of the command to get
   * @return the command at the given index (null if index is out of bounds)
   */
  public ICommand get(int index) {
    if (index >= length || index < 0)
      return null; // Index out of bounds

    Element tmpElement = find(index); // Find the element that should be returned
    return tmpElement.getElement();
  }

  /**
   * Move element at given index up by n steps
   *
   * @param index the index of the element to move up
   * @param n how many steps to move the element
   * @return true if move was successfull, false if index is out of bounds
   */
  public boolean moveUp(int index, int n) {
    if (index >= length || index < 0 || n == 0)
      return false;
    if (n < 0)
      moveDown(index, -n);

    for (int i = 0; i < n; i++) // Move element up
      if (!moveUp(index - i))
        return false;
    return true;
  }

  /**
   * Move element at given index up one step
   *
   * @param index the index of the element to move up
   * @return true if move was successful, false if index is out of bounds
   */
  public boolean moveUp(int index) {
    if (index >= length || index <= 0)
      return false;

    Element e = find(index);
    Element oldPrev = e.prev;
    Element oldNext = e.next;

    e.next = oldPrev;
    e.prev = oldPrev.prev;
    if (oldPrev.prev == null)
      root = e;
    else
      oldPrev.prev.next = e;
    oldPrev.next = oldNext;
    oldPrev.prev = e;
    if (oldNext != null)
      oldNext.prev = oldPrev;

    return true;
  }

  /**
   * Move element at given index down by n steps
   *
   * @param index the index of the element to move down
   * @param n how many steps to move the element
   * @return true if move was successfull, false if index is out of bounds
   */
  public boolean moveDown(int index, int n) {
    if (index >= length || index < 0 || n == 0)
      return false;
    if (n < 0)
      moveUp(index, -n);

    for (int i = 0; i < n; i++) // Move element down n steps
      if (!moveDown(index + i))
        return false;
    return true;
  }

  /**
   * Move element at given index down one step
   *
   * @param index the index of the element to move down
   * @return true if move was successful, false if index is out of bounds
   */
  public boolean moveDown(int index) {
    if (index >= length - 1 || index < 0)
      return false;

    Element e = find(index);
    Element oldPrev = e.prev;
    Element oldNext = e.next;

    e.next = oldNext.next;
    e.prev = oldNext;
    if (oldPrev == null)
      root = oldNext;
    else
      oldPrev.next = oldNext;
    oldNext.prev = oldPrev;
    if (oldNext.next != null)
      oldNext.next.prev = e;
    oldNext.next = e;

    return true;
  }

  /**
   * Clears the list
   */
  public void clear() {
    root = null;
    length = 0;
  }

  public Vector<ICommand> toVector() {
    Vector<ICommand> vec = new Vector<ICommand>();
    for (ICommand cmd : this)
      vec.add(cmd);
    return vec;
  }

  public static void main(String[] args) {
    CommandList list1 = new CommandList();
    CommandList list2 = new CommandList();

    Command c1 = new Gear(100, 0.5);
    list1.add(c1);
    list2.add(c1);
    Command c2 = new Direction(90);
    list1.add(c2);
    list2.add(c2);
    Command c3 = new Pause(0.5);
    list1.add(c3);
    list2.add(c3);
    list1.add(c1);
    list2.add(c1);
    list1.add(c2);
    list2.add(c2);
    list1.add(c3);
    list2.add(c3);

    System.out.println("Liste 1: " + list1);
    System.out.println("Liste 2: " + list2);
    System.out.println("Liste 1 == Liste 2: " + list1.equals(list2));

    list1.moveUp(1);
    list2.moveDown(0);
    list1.remove(0);
    list2.remove(list2.length() - 1);
    System.out.println(list2.get(1));
    System.out.println("Liste 1: " + list1);
    System.out.println("Liste 2: " + list2);
    System.out.println("Liste 1 == Liste 2: " + list1.equals(list2));

  }
}
