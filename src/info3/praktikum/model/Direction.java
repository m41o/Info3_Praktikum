package info3.praktikum.model;

import hsrt.mec.controldeveloper.core.com.command.IDirection;

/**
 * Command that steers in set direction
 *
 * @author Markus Lochmann
 */
public class Direction extends Command implements IDirection {
  /**
   * The name of the command
   */
  public static final String NAME;
  static {
    NAME = Direction.class.getSimpleName();
  }

  /**
   * Maximum steering direction
   */
  private static final int MAX_DEGREE = 90;

  /**
   * Minimum steering direction
   */
  private static final int MIN_DEGREE = -90;
  
  /**
   * Key of first parameter
   */
  public static final String FIRST_PARAM = "Degrees";

  /**
   * Direction to steer in in degrees [-90, 90]
   */
  private int degree = 0;
  
  /**
   * Constructs Command called "Direction" with a standard value of 0
   */
  public Direction() {
    super(NAME);
    params = new String[] {FIRST_PARAM};
    index = CommandType.DIRECTION;
    setDegree(0);
  }

  /**
   * Construct Command called "Direction" and set direction
   *
   * @param degree the number of degrees to set
   */
  public Direction(int degree) {
    this();
    setDegree(degree);
  }

  /**
   * Get steering direction
   *
   * @see hsrt.mec.controldeveloper.core.com.command.IDirection#getDegree()
   *
   * @return number of degrees
   */
  @Override
  public int getDegree() {
    return degree;
  }

  /**
   * Set steering direction
   *
   * @param degree the number of degrees to set [-90, 90]
   */
  public void setDegree(int degree) {
    if (degree >= MIN_DEGREE && degree <= MAX_DEGREE) {
      this.degree = degree;
    }
    else if (degree < MIN_DEGREE)
      this.degree = MIN_DEGREE;
    else
      this.degree = MAX_DEGREE;
  }

  /**
   * Get readable info of Direction
   *
   * @return String representation of Direction
   */
  @Override
  public String toString() {
    if (degree < 0)
      return getName() + ": rotate left " + -degree + " degrees";

    return getName() + ": rotate right " + degree + " degrees";
  }

  @Override
  public String serialize(String separator) {
    return getName() + separator + getDegree();
  }

  @Override
  public void setFirstParam(Number p) {
    setDegree(p.intValue());
  }

  @Override
  public void setSecondParam(Number p) {
    return;
  }

  @Override
  public Number getFirstParam() {
    return new Integer(getDegree());
  }

  @Override
  public Number getSecondParam() {
    return null;
  }

  @Override
  public String parseConfig() {
    return degree + " degrees";
  }

  @Override
  public Number getFirstParamMax() {
    return new Integer(MAX_DEGREE);
  }

  @Override
  public Number getFirstParamMin() {
    return new Integer(MIN_DEGREE);
  }

  @Override
  public Number getSecondParamMax() {
    return null;
  }

  @Override
  public Number getSecondParamMin() {
    return null;
  }
}
