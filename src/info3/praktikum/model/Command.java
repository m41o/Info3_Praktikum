package info3.praktikum.model;

import hsrt.mec.controldeveloper.core.com.command.ICommand;

/**
 * Abstract class describing a command
 *
 * @author Markus Lochmann
 * @author Christoph Jabs (christoph@jabs-family.de)
 */
public abstract class Command implements ICommand {
  /**
   * Name of the command
   */
  private String name;

  /**
   * String of parameter names
   */
  protected String[] params;
  
  /**
   * Index in CommandTypes
   */
  protected int index;
  
  /**
   * Construct command having a name
   *
   * @param name name of the command
   */
  public Command(String name) {
    this.name = name;
  }

  /**
   * get the command's name
   *
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Get the parameters
   *
   * @return the parameters
   */
  public String[] getParams() {
    return params;
  }

  /**
   * Serializes the object into a processable format
   *
   * @param separator Regular expression by which the information is separated
   * @return Serialized object
   */
  public abstract String serialize(String separator);

  /**
   * Get the first parameter of the command
   *
   * @return the first parameter
   */
  public abstract Number getFirstParam();

  /**
   * Get the second parameter of the command
   *
   * @return the second parameter
   */
  public abstract Number getSecondParam();

  /**
   * Set the first parameter of the command
   *
   * @param p Parameter to set
   */
  public abstract void setFirstParam(Number p);

  /**
   * Set the second parameter of the command
   *
   * @param p Parameter to set
   */
  public abstract void setSecondParam(Number p);

  /**
   * Set the two parameters of the command at once.
   * Setters for first and second command need to be
   * implemented by the inheriting class.
   *
   * @param p Array of parameters for the command
   */
  public void setParams(Number[] p) {
    setFirstParam(p[0]);
    setSecondParam(p[1]);
  }

  /**
   * Get the maximum value of first parameter
   * @return the firstParamMax; null if there are no limits
   */
  public abstract Number getFirstParamMax();

  /**
   * Get the minimum value of firstParameter
   * @return the firstParamMin; null if there are no limits
   */
  public abstract Number getFirstParamMin();

  /**
   * Get the maximum value of second parameter
   * @return the secondParamMax; null if there are no limits or no second parameter
   */
  public abstract Number getSecondParamMax();

  /**
   * Get the minimum value of second parameter
   * @return the secondParamMin; null if there are no limits or no second parameter
   */
  public abstract Number getSecondParamMin();

  /**
   * Parse the configuration of the command to a string
   *
   * @return A string representing the configuration of the command
   */
  public abstract String parseConfig();
  
  /**
   * Get the index of the CommandType in commandTypes array
   * 
   * @return the index in CommandTypes
   */
  public int getIndex() {
    return index;
  }
}
