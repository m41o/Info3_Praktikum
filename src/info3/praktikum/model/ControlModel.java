package info3.praktikum.model;

import java.io.File;
import java.util.Vector;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import hsrt.mec.controldeveloper.core.com.ComHandler;
import hsrt.mec.controldeveloper.core.com.IComListener;
import hsrt.mec.controldeveloper.core.com.WiFiCard;
import hsrt.mec.controldeveloper.core.com.WiFiCardHandler;
import hsrt.mec.controldeveloper.core.com.command.ICommand;
import hsrt.mec.controldeveloper.io.IOType;
import hsrt.mec.controldeveloper.io.ObjectFile;
import hsrt.mec.controldeveloper.io.TextFile;

/**
 * Model component of programming software; Implemented as Singleton
 *
 * @author Markus Lochmann
 * @author Christoph Jabs (christoph@jabs-family.de)
 */
public class ControlModel extends CommandList implements IComListener {
  /**
   * String separating the information in serialized commands
   */
  private static final String SEPARATOR = ":";

  /**
   * Only instance of ControlModel (Singleton)
   */
  private static ControlModel instance = null;

  /**
   * Array containing the three command types
   */
  private CommandType[] commandTypes = null;

  /**
   * IOType to load from and save to
   */
  private IOType ioType = null;

  /**
   * Status of the command queue, running or not running
   */
  private boolean status = false;

  /**
   * The communication handler to use
   */
  private ComHandler comHandler = ComHandler.getInstance();

  /**
   * The currently executed command
   */
  protected ICommand currentCommand;

  /**
   * Logger for logging
   */
  private Logger logger = Logger.getLogger(ControlModel.class.getName());

  /**
   * The WiFiCardHandler to use
   */
  private WiFiCardHandler wifiCardHandler;


  /**
   * Private default constructor for ControlModel
   */
  protected ControlModel() {
    createCommandTypes();
    comHandler.register(this);
    wifiCardHandler = new WiFiCardHandler();
  }

  /**
   * Get instance of ControlModel
   *
   * @return instance of ControlModel
   */
  public static ControlModel getInstance() {
    if (instance == null)
      instance = new ControlModel();
    instance.log(Level.INFO, "Instance requested");

    return instance;
  }

  /**
   * @return the ioType
   */
  public IOType getIoType() {
    log(Level.INFO, "IOType requested");
    return ioType;
  }

  /**
   * @param ioType the ioType to set
   */
  public void setIoType(IOType ioType) {
    this.ioType = ioType;
    log(Level.INFO, "IOType set");
  }

  /**
   * @return the status
   */
  public boolean isStatus() {
    return status;
  }

  /**
   * Set the ioType to an ObjectFile or TextFile
   *
   * @param file Path to the file
   * @param append true: append to file; false: overwrite file
   * @param serialized true: set to ObjectFile; false: set to TextFile
   */
  public void setIoType(File file, boolean append, boolean serialized) {
    if (serialized)
      setIoType(new ObjectFile(file, append));
    else
      setIoType(new TextFile(file, append));
  }

  /**
   * Creates a CommandType object for each command and stores them in the array commandTypes
   */
  private void createCommandTypes() {
    commandTypes = new CommandType[3];
    commandTypes[CommandType.DIRECTION] = new CommandType(Direction.NAME);
    commandTypes[CommandType.GEAR] = new CommandType(Gear.NAME);
    commandTypes[CommandType.PAUSE] = new CommandType(Pause.NAME);
  }

  /**
   * Get commandTypes array
   *
   * @return Array of all possible command types
   */
  public CommandType[] getCommandTypes() {
    return commandTypes;
  }

  /**
   * Load data from set IOType
   *
   * @return false if error occurred, true otherwise
   */
  public boolean load() {
    return load(ioType, false);
  }

  /**
   * Save data to set IOType
   *
   * @return false if error occurred, true otherwise
   */
  public boolean save() {
    return save(ioType);
  }

  /**
   * Load data from IO stream
   *
   * @param ioType The IOType object to read from
   * @param append True to append to current data, false to overwrite
   * @return false if error occurred, true otherwise
   */
  private boolean load(IOType ioType, boolean append) {
    Vector<String> data = new Vector<String>();

    if (ioType.read(data)) {
      ioType.close();

      // Clear the list
      if (!append)
        this.clear();

      // Iterate through data and create a Command for every String
      for (String str : data) {
        String[] strs = str.split(SEPARATOR); // Split Strings at separator
        Command cmd = (Command) CommandType.createInstance(strs[0]); // Instantiate corresponding
                                                                     // command

        if (cmd != null) {
          Number[] params = {0.0, 0.0};

          try {
            params[0] = new Double(strs[1]);
          } catch (Exception e) {
          }

          try {
            params[1] = new Double(strs[2]);
          } catch (Exception e) {
          }

          cmd.setParams(params);
          this.add((ICommand) cmd);
        }
      }
      log(Level.INFO, "Loaded data");
      return true;
    }

    ioType.close();
    log(Level.INFO, "Failed to load data");
    return false;
  }

  /**
   * Save data to IO stream
   *
   * @param ioType The IOType object data is written to
   * @return false if error occurred, true otherwise
   */
  private boolean save(IOType ioType) {
    if (ioType.write(serialize())) {
      ioType.close();
      log(Level.INFO, "Wrote data");
      return true;
    }

    ioType.close();
    log(Level.INFO, "Failed to write data");
    return false;
  }

  /**
   * Load data from File
   *
   * @param file The abstract file path to read from
   * @param append True to append to current data, false to overwrite
   * @param serialized true: read from ObjectFile; false: read from TextFile
   * @return false if error occurred, true otherwise
   */
  public boolean load(File file, boolean append, boolean serialized) {
    IOType ioType;

    if (serialized)
      ioType = new ObjectFile(file, false);
    else
      ioType = new TextFile(file, false);

    return load(ioType, append);
  }

  /**
   * Load new data list from file and overwrite current data
   *
   * @param file The abstract file path to read from
   * @param serialized true: read from ObjectFile; false: read from TextFile
   * @return false if error occurred, true otherwise
   */
  public boolean load(File file, boolean serialized) {
    return load(file, false, serialized);
  }

  /**
   * Save data to File
   *
   * @param file The abstract file path data is written to
   * @param append true: append to file; false: overwrite file
   * @param serialized true: write to ObjectFile; false: write to TextFile
   * @return false if error occurred, true otherwise
   */
  public boolean save(File file, boolean append, boolean serialized) {
    IOType ioType;

    if (serialized)
      ioType = new ObjectFile(file, append);
    else
      ioType = new TextFile(file, append);

    return save(ioType);
  }

  /**
   * Method called by ComHandler
   *
   * @param command The last command performed by the ComHandler
   */
  @Override
  public void commandPerformed(ICommand command) {
    int commandIndex = includes(command);
    if (commandIndex >= length() - 1)
      status = false;
    else
      currentCommand = command;
    log(Level.INFO, "Command performed");
  }

  /**
   * Get the list of commands
   *
   * @return controlProcess
   */
  public CommandList getControlProcess() {
    return (CommandList) this;
  }

  /**
   * Serializes the content of the CommandList to a Vector of Strings
   *
   * @return Serialized CommandList
   */
  private Vector<String> serialize() {
    Vector<String> data = new Vector<String>();

    for (ICommand cmd : this) {
      data.add(((Command) cmd).serialize(SEPARATOR));
    }
    return data;
  }

  /**
   * @return Array of available WifiCards
   */
  public WiFiCard[] getWiFiCards() {
    return wifiCardHandler.getWiFiCards();
  }

  /**
   * Toggle the status of the command queue between running and not running
   *
   * @return The new status of the command queue
   */
  public boolean toggleStatus() {
    if (status) {
      if (comHandler.stop()) {
        currentCommand = null;
        logger.info("Stopped ComHandler");
        return status = false;
      } else { // Stopping unsuccessful
        logger.info("Failed to stop ComHandler");
        return true;
      }
    }
    if (length() > 0)
      if (comHandler.start(this.toVector(), ioType)) {
        logger.info("Started ComHandler");
        return status = true;
      } else { // Starting unsuccessful
        logger.info("Failed to start ComHandler");
        return false;
      }
    return false; // No commands in list
  }

  /**
   * Add a new log handler
   *
   * @param logHandler The log handler to add
   */
  public void addLogHandler(Handler logHandler) {
    logger.addHandler(logHandler);
  }

  /**
   * Remove a given log handler
   *
   * @param logHandler The log handler to remove
   */
  public void removeLogHandler(Handler logHandler) {
    logger.removeHandler(logHandler);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void clear() {
    super.clear();
    log(Level.INFO, "Cleared List");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean add(ICommand newCommand) {
    boolean returnVal = super.add(newCommand);
    log(Level.INFO, "Added Command");
    return returnVal;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean moveDown(int index, int n) {
    boolean returnVal = super.moveDown(index, n);
    log(Level.INFO, "Moved command at position " + index + " down " + n + " steps");
    return returnVal;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean moveDown(int index) {
    boolean returnVal = super.moveDown(index);
    log(Level.INFO, "Moved command at position " + index + " down");
    return returnVal;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean moveUp(int index, int n) {
    boolean returnVal = super.moveUp(index, n);
    log(Level.INFO, "Moved command at position " + index + " up " + n + " steps");
    return returnVal;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean moveUp(int index) {
    boolean returnVal = super.moveUp(index);
    log(Level.INFO, "Moved command at position " + index + " up");
    return returnVal;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean remove(int index) {
    boolean returnVal = super.remove(index);
    log(Level.INFO, "Removed command at position " + index);
    return returnVal;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean replace(ICommand oldCmd, ICommand newCmd) {
    boolean returnVal = super.replace(oldCmd, newCmd);
    log(Level.INFO, "Replaced a command");
    return returnVal;
  }

  /**
   * Log a message to the configured logger
   *
   * @param level The log level
   * @param msg The log message
   */
  protected void log(Level level, String msg) {
    logger.log(level, msg);
  }
}
