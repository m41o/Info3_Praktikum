package info3.praktikum.model;

import hsrt.mec.controldeveloper.core.com.command.IPause;

/**
 * Command that pauses for set duration
 *
 * @author Markus Lochmann
 */
public class Pause extends Command implements IPause {
  /**
   * The name of the command
   */
  public static final String NAME;
  static {
    NAME = Pause.class.getSimpleName();
  }
  
  /**
   * Key for first Parameter
   */
  public static final String FIRST_PARAM = "Duration";
  
  /**
   * Duration to pause for in seconds
   */
  private double duration;

  /**
   * Construct Command called "Pause" and set standard duration of 100
   */
  public Pause() {
    super(NAME);
    params = new String[] {FIRST_PARAM};
    index = CommandType.PAUSE;
    setDuration(0.0);
  }

  /**
   * Construct command called "Pause" and set duration
   *
   * @param duration the duration to set
   */
  public Pause(double duration) {
    this();
    setDuration(duration);
  }

  /**
   * Get duration of pause
   *
   * @see hsrt.mec.controldeveloper.core.com.command.IPause#getDuration()
   *
   * @return duration
   */
  @Override
  public double getDuration() {
    return duration;
  }

  /**
   * Set duration to pause for
   *
   * @param duration the duration to set
   */
  public void setDuration(double duration) {
    if (duration > 0)
      this.duration = duration;
    else
      this.duration = 0.0;
  }

  /**
   * Get readable info of Pause
   *
   * @return String representation of Pause
   */
  @Override
  public String toString() {
    return getName() + " for " + duration + " seconds";
  }

  @Override
  public String serialize(String separator) {
    return getName() + separator + getDuration();
  }

  /**
   * Set duration to pause for
   *
   * @param p the duration to set
   */
  @Override
  public void setFirstParam(Number p) {
    setDuration(p.doubleValue());
  }

  /**
   * Does nothing
   *
   * @param p Parameter to set
   */
  @Override
  public void setSecondParam(Number p) {
    return;
  }

  @Override
  public Number getFirstParam() {
    return new Double(getDuration());
  }

  @Override
  public Number getSecondParam() {
    return null;
  }

  @Override
  public String parseConfig() {
    return duration + "s";
  }

  @Override
  public Number getFirstParamMax() {
    return null;
  }

  @Override
  public Number getFirstParamMin() {
    return new Double(0.0);
  }

  @Override
  public Number getSecondParamMax() {
    return null;
  }

  @Override
  public Number getSecondParamMin() {
    return null;
  }
}
